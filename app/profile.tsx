import { Redirect, Stack } from "expo-router";
import { Pressable, StyleSheet, Text, View } from "react-native";
import { useEffect, useState } from "react";
import { Session } from "@supabase/supabase-js";
import { supabase } from "../lib/supabase";
import { useAuth } from "../providers/AuthProvider";
export default function ProfileScreen() {
  const { user } = useAuth();
  if (!user) {
    return <Redirect href={"/login"} />;
  }

  return (
    <View style={styles.container}>
      <Stack.Screen options={{ title: "User Profile" }} />
      <Text> User id:{user?.id} </Text>
      <Text> Email:{user?.email} </Text>
      <Pressable style={styles.button} onPress={() => supabase.auth.signOut()}>
        <Text style={styles.buttontext}>Sign Out</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "gainsboro",
    flex: 1,
    gap: 16,
    justifyContent: "center",
  },
  inputContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    borderRadius: 5,
    paddingHorizontal: 6,
    alignItems: "center",
  },
  textInput: {
    padding: 6,
  },
  button: {
    padding: 12,
    backgroundColor: "black",
    borderRadius: 5,
  },
  buttonDisabled: {
    backgroundColor: "gray",
  },
  buttontext: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
  },
});
