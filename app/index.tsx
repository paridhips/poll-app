import { Stack } from "expo-router";
import { Alert, FlatList, StyleSheet, Text, View } from "react-native";
import { Link } from "expo-router";
import { MaterialIcons } from "@expo/vector-icons";
import { useEffect, useState } from "react";
import { supabase } from "../lib/supabase";
import { Database } from "../types/supabase";
import { Poll } from "../types/db";


export default function Page() {
  const [polls, setPolls] = useState<Poll[]>([]);
  const [isFetching, setIsFetching] = useState(false);
  const fetchPolls = async () => {
    console.log("Fetching...");
    setIsFetching(true);

    let { data, error } = await supabase.from("polls").select("*");
    if (error) {
      Alert.alert("Error Fetching Data");
      setIsFetching(false);
    }

    setIsFetching(false);
    console.log(data);
    setPolls(data);
  };

  useEffect(() => {
    fetchPolls();
  }, []);
  return (
    <>
      <Stack.Screen
        options={{
          headerTitleAlign: "center",
          title: "Home",
          headerRight: () => (
            <Link href={"/polls/new"}>
              <MaterialIcons name="add" size={24} color="black" />
            </Link>
          ),
          headerLeft: () => (
            <Link href={"/login"}>
              <MaterialIcons
                style={{ paddingRight: 12 }}
                name="person"
                size={24}
                color="black"
              />
            </Link>
          ),
        }}
      />
      <FlatList
        onRefresh={fetchPolls}
        refreshing={isFetching}
        style={styles.flatListStyle}
        data={polls}
        contentContainerStyle={styles.container}
        renderItem={({ item }) => (
          <Link style={styles.pollContainer} href={`/polls/${item.id}`}>
            <Text style={styles.pollTitle}>{item.question}</Text>
          </Link>
        )}
      />
    </>
  );
}

const styles = StyleSheet.create({
  flatListStyle: {
    backgroundColor: "gainsboro",
  },
  container: {
    flex: 1,
    padding: 10,
    gap: 5,
  },
  pollContainer: {
    backgroundColor: "#fff",
    padding: 10,
    borderRadius: 5,
  },
  pollTitle: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
