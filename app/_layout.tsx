import { Slot, Stack } from "expo-router";
import Toast from "react-native-toast-message";
import AuthProvider from "../providers/AuthProvider";

export default function RootLayout() {
  return (
    <AuthProvider>
      <Stack />
      <Toast/>
    </AuthProvider>
  );
}
