import { Stack } from "expo-router";
import { useState } from "react";
import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { useAuth } from "../../providers/AuthProvider";
import { Redirect } from "expo-router";
import Toast from "react-native-toast-message";
import { supabase } from "../../lib/supabase";
import { Bar } from "react-native-progress";

export default function NewPoll() {
  const [question, setQuestion] = useState("");
  const [options, setOptions] = useState(["", ""]);
  const [loading, setLoading] = useState(false);
  const { user } = useAuth();
  if (!user) {
    return <Redirect href={"/login"} />;
  }

  const updateOptions = (text, index) => {
    const updated = [...options];
    updated[index] = text;
    setOptions(updated);
  };
  const validateInput = () => { };
  const createPoll = async () => {
    setLoading(true);
    const validOptions = options.filter((o) => !!o);
    if (!question) {
      Toast.show({
        type: "error",
        text1: "Error!",
        text2: "Please enter a question!",
      });
      setLoading(false);
      return;
    } else if (validOptions.length < 2) {
      Toast.show({
        type: "error",
        text1: "Error!",
        text2: "Please enter atleat two valid options!",
      });
      setLoading(false);
      return;
    }

    const { data, error } = await supabase
      .from("polls")
      .insert([{ question, options:validOptions }])
      .select();
    if (error) {
      Toast.show({
        type: "error",
        text1: "Error!",
        text2: error.message,
      });
      setLoading(false);
    }
    if (data) {
      Toast.show({
        type: "success",
        text1: "Success!",
        text2: "Poll Created Successfully",
      });
      setQuestion("");
      setOptions(["", ""]);
      setLoading(false);
    }
  };

  return (
    <>
      <Stack.Screen options={{ title: "Create a New Poll" }} />
      <View style={styles.container}>
        <Text style={styles.label}>New Poll</Text>
        <TextInput
          value={question}
          onChangeText={setQuestion}
          style={styles.input}
          placeholder="Type your question here"
        />
        <Text style={styles.label}>Options</Text>
        {options.map((option, index) => (
          <View key={index} style={styles.optionContainer}>
            <TextInput
              style={styles.optionInput}
              placeholder={`Option ${index + 1}`}
              value={option}
              onChangeText={(text) => {
                updateOptions(text, index);
              }}
            />
            <MaterialIcons
              style={styles.optionDelete}
              name="delete"
              size={18}
              onPress={() => {
                const updated = [...options];
                updated.splice(index, 1);
                setOptions(updated);
              }}
            />
          </View>
        ))}
        <Pressable
          disabled={loading}
          onPress={() => setOptions([...options, ""])}
          style={[styles.transButton, loading && styles.buttonDisabled]}
        >
          <MaterialIcons name="add" size={24} />
        </Pressable>
        <Pressable
          onPress={createPoll}
          style={[styles.button, loading && styles.buttonDisabled]}
          disabled={loading}
        >
          <Text style={styles.buttonText}>Create Poll</Text>
        </Pressable>
        <Bar
          indeterminate={loading}
          width={null}
          borderColor="gainsboro"
          color="black"
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    gap: 5,
    flex:1,
    backgroundColor:'gainsboro'
  },
  optionDelete: {
    padding: 10,
  },
  buttonDisabled: {
    backgroundColor: "gray",
  },

  label: {
    fontWeight: "500",
    marginTop: 10,
  },
  optionContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    borderRadius: 5,
  },
  optionInput: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 5,
    width: "90%",
  },

  input: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 5,
  },
  button: {
    marginTop: 12,
    borderRadius: 5,
    backgroundColor: "black",
  },
  buttonText: {
    padding: 12,
    fontWeight: "bold",
    textAlign: "center",
    color: "white",
  },
  transButton: {
    marginTop: 12,
    borderRadius: 5,
    borderWidth: 1,
    padding: 8,
    backgroundColor: "white",
    borderColor: "black",
    alignItems: "center",
  },
});
