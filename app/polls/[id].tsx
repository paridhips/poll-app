import { Stack, useLocalSearchParams } from "expo-router";
import {
  View,
  Text,
  StyleSheet,
  Pressable,
  ActivityIndicator,
} from "react-native";
import { Feather } from "@expo/vector-icons";
import { useEffect, useState } from "react";
import { Poll } from "../../types/db";
import Toast from "react-native-toast-message";
import { supabase } from "../../lib/supabase";
export default function PollDetails() {
  const { id } = useLocalSearchParams();
  const [selected, setSelected] = useState("React Native");
  const [poll, setPoll] = useState<Poll>(null);
  const fetchPoll = async () => {
    console.log("Fetching...");

    let { data, error } = await supabase.from("polls").select("*").eq("id", id).single();
    if (error) {
      Toast.show({
        type: "error",
        text1: "Error!",
        text2: "Error fetching data!",
      });
    }

    console.log(data);
    //setPolls(data);
    setPoll(data);
  };

  useEffect(() => {
    fetchPoll();
  }, []);

  const vote = () => {
    Toast.show({
      type:'success',
      text1:poll.question,
      text2:selected,
    })
  };
  if (!poll) {
    return (
      <>
        <Stack.Screen options={{ title: "Poll Details" }} />
        <ActivityIndicator
          size={60}
          color={"black"}
          style={styles.activityIndicator}
        />
      </>
    );
  }

  return (
    <>
      <Stack.Screen options={{ title: "Poll Details" }} />
      <View style={styles.container}>
        <Text style={styles.question}>{poll.question}</Text>
        <View style={styles.optionContainer}>
          {poll.options.map((option) => (
            <Pressable
              onPress={() => setSelected(option)}
              style={styles.optinItemContainer}
              key={option}
            >
              <Feather
                name={option === selected ? "check-circle" : "circle"}
                size={18}
                color={option === selected ? "green" : "gray"}
              />
              <Text style={styles.optionText}>{option}</Text>
            </Pressable>
          ))}
        </View>
        <Pressable onPress={vote} style={styles.button}>
          <Text style={styles.buttonText}>Submit</Text>
        </Pressable>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 16,
    gap: 20,
  },
  question: {
    fontSize: 20,
    fontWeight: "600",
  },
  optionContainer: {
    gap: 5,
  },
  activityIndicator: {
    marginVertical: "auto",
  },
  optinItemContainer: {
    flexDirection: "row",
    gap: 10,
    backgroundColor: "white",
    padding: 10,
    borderRadius: 5,
  },
  optionText: {},
  button: {
    backgroundColor: "black",
    borderRadius: 5,
  },
  buttonText: {
    color: "white",
    padding: 12,
    textAlign: "center",
    fontWeight: "bold",
  },
});
