import { Redirect, Slot, Stack } from "expo-router";
import {
  View,
  Text,
  StyleSheet,
  Pressable,
  TextInput,
  AppState,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { useState } from "react";
import { supabase } from "../../lib/supabase";
import { Bar } from "react-native-progress";
import Toast from "react-native-toast-message";
import { useAuth } from "../../providers/AuthProvider";
AppState.addEventListener("change", (state) => {
  if (state === "active") {
    supabase.auth.startAutoRefresh();
  } else {
    supabase.auth.stopAutoRefresh();
  }
});

export default function LoginScreen() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const {user}=useAuth();
  if(user)
{
    return <Redirect href={"/profile"}/>
  }
  async function signInWithEmail() {
    setLoading(true);
    const { error } = await supabase.auth.signInWithPassword({
      email,
      password,
    });
    if (error) {
      Toast.show({
        type: "error",
        text1: "Error!",
        text2: error.message,
      });
    }
    setLoading(false);
  }

  async function signUpWithEmail() {
    setLoading(true);
    const {
      data: { session },
      error,
    } = await supabase.auth.signUp({ email, password });
    if (error) {
      Toast.show({
        type: "error",
        text1: "Error!",
        text2: error.message,
      });
    } else if (!session) {
      Toast.show({
        type: "success",
        text1: "Success!",
        text2: "Please Check Your Email for Verification Message",
      });
    }
    setLoading(false);
  }
  return (
    <>
      <Stack.Screen options={{title:'Login'}}/>
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <MaterialIcons name="alternate-email" size={18} />
          <TextInput
            value={email}
            onChangeText={setEmail}
            inputMode="email"
            style={styles.textInput}
            placeholder="Email"
          />
        </View>
        <View style={styles.inputContainer}>
          <MaterialIcons name="password" size={18} />
          <TextInput
            value={password}
            onChangeText={setPassword}
            inputMode="text"
            secureTextEntry={true}
            style={styles.textInput}
            placeholder="Password"
          />
        </View>
        <Pressable
          onPress={signInWithEmail}
          disabled={loading}
          style={[styles.button, loading && styles.buttonDisabled]}
        >
          <Text style={styles.buttontext}>Log In</Text>
        </Pressable>
        <Pressable
          disabled={loading}
          style={[styles.button, loading && styles.buttonDisabled]}
          onPress={signUpWithEmail}
        >
          <Text style={styles.buttontext}>Sign Up</Text>
        </Pressable>
        <Bar
          indeterminate={loading}
          width={null}
          borderColor="gainsboro"
          color="black"
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "gainsboro",
    flex: 1,
    gap: 16,
    justifyContent: "center",
  },
  inputContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    borderRadius: 5,
    paddingHorizontal: 6,
    alignItems: "center",
  },
  textInput: {
    padding: 6,
  },
  button: {
    padding: 12,
    backgroundColor: "black",
    borderRadius: 5,
  },
  buttonDisabled: {
    backgroundColor: "gray",
  },
  buttontext: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
  },
});
